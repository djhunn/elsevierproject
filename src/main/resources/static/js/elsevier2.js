$(".ba").click(function () {
    if ($(".ba").is(":checked")) {
        $(".tex")
            .removeAttr("disabled")
            .css("background-color", "white");
    }
    else {
        $(".tex")
            .attr("disabled", "disabled")
            .css("background-color", "red");
    }
});
function FillBilling(f) {
	  if(f.billingSame.checked == true) {
	    f.firstName.value = f.shippingFirstName.value;
	    f.lastName.value = f.shippingLastName.value;
	    f.addressLine1.value = f.shippingAddressLine1.value;
	    f.addressLine2.value = f.shippingAddressLine2.value;
	    f.city.value = f.shippingCity.value;
	    f.county.value = f.shippingCounty.value;
	    f.postcode.value = f.shippingPostcode.value;
	  }
	  else{
		  f.firstName.value = "";
		  f.lastName.value = "";
		  f.addressLine1.value = "";
		  f.addressLine2.value = "";
	      f.city.value = "";
		  f.county.value = "";
		  f.postcode.value = "";
	  }
	}
function setQuantity(f) {
	  f.userQuantity.value=f.quantity.value;
	}

