<!doctype html>
<%@page import="com.qa.models.Customer"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.qa.models.Book"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.io.File"%>
<%@page import="java.util.Random"%>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Home</title>
<%@include file="style_files.jsp"%>
</head>
<body class="main">
	<%
		NumberFormat formatter = new DecimalFormat("#0.00");
	%>
	<%@include file="navigation.jsp"%>



	<div class="whitebg">
		<div class="slideshow-container">
			<div class="mySlides fade">
				<div class="numbertext">1 / 5</div>
				<img src="images/familyReadingCrop.png" alt="Family reading">
				<div class="text" style="background-color: rgb(128, 0, 0);"
					id="3d-slide">
					<strong>There are many ways to enjoy reading... with
						family...</strong>
				</div>
			</div>

			<div class="mySlides fade">
				<div class="numbertext">2 / 5</div>
				<img src="images/slideshowPooh.png" alt="Winnie the Pooh reading">
				<div class="text" style="background-color: rgb(128, 0, 0);"
					id="3d-slide">
					<strong>...with friends...</strong>
				</div>
			</div>

			<div class="mySlides fade">
				<div class="numbertext">3 / 5</div>
				<img src="images/slideshowWine.png" alt="Reading with wine">
				<div class="text" style="background-color: rgb(128, 0, 0);"
					id="3d-slide">
					<strong>...with wine...</strong>
				</div>
			</div>

			<div class="mySlides fade">
				<div class="numbertext">4 / 5</div>
				<img src="images/slideshowQuentin.png" alt="Roald Dahl graphic">
				<div class="text" style="background-color: rgb(128, 0, 0);"
					id="3d-slide">
					<strong>...or just lost in your mind.</strong>
				</div>
			</div>

			<div class="mySlides fade">
				<div class="numbertext">5 / 5</div>
				<img src="images/slideshowGirl.png" alt="Girl reading">
				<div class="text" style="background-color: rgb(128, 0, 0);"
					id="3d-slide">
					<strong>But whatever the situation, we all love being
						surrounded by <i>Proper Good Books</i>.
					</strong>
				</div>
			</div>



			<a class="prev" onclick="plusSlides(-1)">&#10094;</a> <a class="next"
				onclick="plusSlides(1)">&#10095;</a>
		</div>
		<br>

		<div style="display: none;">
			<span class="dot" onclick="currentSlide(1)"></span> <span class="dot"
				onclick="currentSlide(2)"></span> <span class="dot"
				onclick="currentSlide(3)"></span> <span class="dot"
				onclick="currentSlide(4)"></span> <span class="dot"
				onclick="currentSlide(3)"></span>
		</div>

		<div class="row column text-center">
			<p>Here is a selection of our latest releases;
				head over to our <a href="/bookPortal">Book Portal</a> for more
				information on the wide variety of reading materials we have on
				offer!


				<%
				Iterable<Book> books = (Iterable<Book>) session.getAttribute("books");
			%>


			</p>
			<hr>
		</div>

		<div class="row small-up-3 medium-up-6 large-up-9">

			<%
				//This section sets up the process to display a random 12 book covers on the home page

				//Gets an object from which we can extract random numbers
				Random rand = new Random();

				//Creates a list called "bookList" containing all the books in the database					
				List<Book> bookList = new ArrayList<Book>();
				for (Book book : books) {
					bookList.add(book);
				}

				//Creates an initially empty list which will be populated with 12 random books whose covers will be displayed
				List<Book> booksToDisplay = new ArrayList<Book>();

				//Populates the list with random books
				for (int m = 1; m <= 12; m++) { //Iterates 12 times, each time adding 1 book to the list
					int random = rand.nextInt(bookList.size()); //Gets a random number which will be used as an index, with which we will extract a book from bookList
					booksToDisplay.add(bookList.get(random)); //Adds the corresponding book in bookList to booksToDisplay
					bookList.remove(random); //Removes the chosen book from bookList so that we can't choose it again (if we could, then there would be duplicates on the home page).
					//Note that the size of bookList has now decreased by 1, which is why we chose a random number between 0 and bookList.size()-1 (inclusive)
				}

				for (Book book : booksToDisplay) {
			%>
			<div class="column">

				<%
					File f = new File("src/main/resources/static/" + book.getBookImage());

						File folder = new File("src/main/resources/static/images");
						File[] listOfFiles = folder.listFiles();

						for (int i = 0; i < listOfFiles.length; i++) {

							if (f.getPath().equals(listOfFiles[i].getPath())) {
				%>
				<a href="/bookDetails?bookId=<%=book.getBookId()%>"><img
					class="thumbnail" src="<%=book.getBookImage()%>" height=300px
					width=450px alt="Book cover"></a>
				<%
					break;
							}

							if (i == listOfFiles.length - 1) {
				%><a
					href="/bookDetails?bookId=<%=book.getBookId()%>"><img
					class="thumbnail" src="images/PlaceholderBook.png" height=300px
					width=450px alt="Placeholder cover"></a>
				<%
					}
						}
				%>
			</div>

			<%
				}
			%>

		</div>
		<%@include file="custom_footer.jsp"%>
	</div>
	<%@include file="script_files.jsp"%>
	<script>
		var slideIndex = 1;
		showSlides(slideIndex);

		function plusSlides(n) {
			showSlides(slideIndex += n);
		}

		function currentSlide(n) {
			showSlides(slideIndex = n);
		}

		function showSlides(n) {
			var i;
			var slides = document.getElementsByClassName("mySlides");
			var dots = document.getElementsByClassName("dot");
			if (n > slides.length) {
				slideIndex = 1
			}
			if (n < 1) {
				slideIndex = slides.length
			}
			for (i = 0; i < slides.length; i++) {
				slides[i].style.display = "none";
			}
			for (i = 0; i < dots.length; i++) {
				dots[i].className = dots[i].className.replace(" active", "");
			}
			slides[slideIndex - 1].style.display = "block";
			dots[slideIndex - 1].className += " active";
		}
		var slideIndex = 0;
		carousel();

		function carousel() {
			var i;
			var x = document.getElementsByClassName("mySlides");
			for (i = 0; i < x.length; i++) {
				x[i].style.display = "none";
			}
			slideIndex++;
			if (slideIndex > x.length) {
				slideIndex = 1
			}
			x[slideIndex - 1].style.display = "block";
			setTimeout(carousel, 4000);
		}
	</script>
</body>
</html>
