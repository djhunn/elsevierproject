<!doctype html>
<%@page import="com.qa.models.Customer"%>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Your Profile</title>
<%@include file="style_files.jsp"%>
</head>
<body class="main">



	<%@include file="navigation.jsp"%>
	<div class="whitebg">
		<div>
			<div class="row column text-center">
<br>
				<h3>
					You have logged in as
					<%=c.getFirstName()%></h3>
			</div>


			<div class="row column">

				<form action="/updateProfile" method="post">
					<div class="medium-6">
						<h3>Account Details</h3>


						<label>First Name</label> <input type="text"
							placeholder="Enter First Name" name="firstName" id="firstName"
							value="<%=c.getFirstName()%>" pattern="[A-Za-z\s-]{1,}"
							title="Input must be non-empty and only include letters/ hyphens"
							required /> <label>Last Name</label> <input type="text"
							placeholder="Enter Last Name" name="lastName" id="lastName"
							value="<%=c.getLastName()%>" pattern="[A-Za-z\s-]{1,}"
							title="Input must be non-empty and only include letters/ hyphens"
							required /> <label>Email Address</label> <input type="email"
							placeholder="Enter Email" name="email" id="email"
							value="<%=c.getEmail()%>"
							title="Input must be a valid email address" required /><label>Password*</span> </label>
				 <input type="password" placeholder="Enter Password" name="password" id="password" value="<%=c.getPassword()%>" required/>
				 <label>Confirm Password*</span> </label>
				 <input type="password" placeholder="Confirm Password" name="passwordC" id="passwordC" value="<%=c.getPasswordC()%>"required/>
            

					</div>



					<input type="submit" class="button create account"
						value="Update Account Details">

				</form>

			</div>







		</div>
		<%@include file="custom_footer.jsp"%>
	</div>
	<%@include file="script_files.jsp"%>
</body>
</html>


