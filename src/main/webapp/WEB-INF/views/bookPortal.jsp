<!doctype html>
<%@page import="com.qa.models.Customer"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.qa.models.Book"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.io.File" %>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Book Portal</title>
<%@include file="style_files.jsp"%>
</head>
<body class="main">
	<%
		NumberFormat formatter = new DecimalFormat("#0.00");
	%>
	<%@include file="navigation.jsp"%>

	<div class="whitebg">
		<div class="row column text-center">
			<br><h2>
				Here is our complete catalogue


				<%
				Iterable<Book> books = (Iterable<Book>) session.getAttribute("books");
			%>


			</h2>
			<hr>
		</div>

		<div class="row small-up-1 medium-up-2 large-up-4">

			<%
				for (Book book : books) {
			%>
			<div class="column">

				<% File f = new File("src/main/resources/static/"+book.getBookImage());
								
				File folder = new File("src/main/resources/static/images");
				File[] listOfFiles = folder.listFiles();
				
				for(int i=0;i<listOfFiles.length;i++){
					
					if (f.getPath().equals(listOfFiles[i].getPath())) {
					System.out.println("thinks pic exists");%>
			    	  <a href="/bookDetails?bookId=<%=book.getBookId()%>"><img
						class="thumbnail" src="<%=book.getBookImage()%>" height=300px
						width=450px alt="Book cover"></a><%
						break;}
					
			      if (i==listOfFiles.length-1) {System.out.println("thinks pic doesn't exist");%><a href="/bookDetails?bookId=<%=book.getBookId()%>"><img
				class="thumbnail" src="images/PlaceholderBook.png" height=300px
				width=450px alt="Placeholder cover"></a><%
			      } 
				} %>
				<h6><%=book.getTitle()%></h6>
				<p>
					�<%=formatter.format(book.getPrice())%>
				</p>
				<a href="/bookDetails?bookId=<%=book.getBookId()%>"
					class="button expanded">View book details</a>
				<!--  a href="/addToCart?bookId=" class="button expanded">Add to Cart</a>-->
			</div>

			<%
				}
			%>
		</div>

		<%@include file="custom_footer.jsp"%>
	</div>
	<%@include file="script_files.jsp"%>
</body>
</html>
