<!doctype html>
<%@page import="java.util.Map"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.qa.models.Book"%>
<%@page import="com.qa.models.Shipping"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Confirmation &amp; Payment</title>
<%@include file="style_files.jsp"%>
<%NumberFormat formatter = new DecimalFormat("#0.00");%>

</head>
<body class="main">
	<%@include file="navigation.jsp"%>
	<div class="whitebg">


		<%
			double orderTotal = (Double) request.getAttribute("order_total");
			Shipping currentShipping = (Shipping) request.getAttribute("shipping_address");
		%>

		<br>
		<!-- You can now combine a row and column if you just need a 12 column row -->
		<div class="row columns">
			<nav aria-label="You are here:" role="navigation">
				<ul class="breadcrumbs">

					<li><a href="/">Home</a></li>
					<li><span class="show-for-sr">Current: </span> Payment Details
					</li>
				</ul>
			</nav>
		</div>



		<div class="row columns">
			<h2>Confirmation &amp; Payment</h2>
			<br>
			<h6>Please enter your payment details, including billing address, then review you order and confirm.</h6>
		</div>

			<form action="/paymentProcess" method="post">
			<div class="row" id="confirmation">
				<div class="small-12 medium-6 columns" id="shippingDetails">
				<h3>Shipping Address</h3>
					<div class="row" id="contactDetails">
						<div class="small-12 medium-6 large-6 columns" id="firstAndLastName">
							<label> First name * </label><input type="text"
								name="shippingFirstName"
								value="<%=currentShipping.getFirstName()%>" readonly> <label>
								Last name * </label><input type="text" name="shippingLastName"
								value="<%=currentShipping.getLastName()%>" readonly>
						</div>

						<div class="small-12 medium-6 large-6 columns" id="phoneAndEmail">
							<label> Phone Number</label><input type="text" name="phone"
								value="<%=currentShipping.getPhone()%>" readonly> <label>
								Email * </label><input type="text" name="email"
								value="<%=currentShipping.getEmail()%>" readonly>
						</div>
					</div>

					<div class="row" id="sAddressDetails">

						<label> Address Line 1 * </label> <input type="text"
							name="shippingAddressLine1"
							value="<%=currentShipping.getAddressLine1()%>" readonly>
						<label>Address Line 2 * </label> <input type="text"
							name="shippingAddressLine2"
							value="<%=currentShipping.getAddressLine2()%>" readonly>

						<div class="row" id="cityPostcodeCounty">
							<div class="small-4 medium-4 large-4 columns">
								<label>City/Town * </label> <input type="text"
									name="shippingCity" value="<%=currentShipping.getCity()%>"
									readonly>
							</div>
							<div class="small-4 medium-4 large-4 columns">
								<label>County * </label> <input type="text"
									name="shippingCounty" value="<%=currentShipping.getCounty()%>"
									readonly>
							</div>
							<div class="small-4 medium-4 large-4 columns">
								<label>Postcode * </label> <input type="text"
									name="shippingPostcode"
									value="<%=currentShipping.getPostcode()%>" readonly>
							</div>


						</div>
					</div>

					<div class="row" id="sameAddressCheck">
						<div class="column">
							<input type="checkbox" name="billingSame" id="same"
								onclick="FillBilling(this.form)" /> My billing and shipping
							address are the same
						</div>
					</div>
				</div>
			
				<div class="small-12 medium-6 columns" id="billingAddressDetails">
				<h3>Billing Address</h3>
					<div class="row" id="billingAddressContact">
						<div class="small-12 medium-6 large-6 columns">
							<label> First name * </label><input type="text" name="firstName">
						</div>
						<div class="small-12 medium-6 large-6 columns">
							<label> Last name * </label><input type="text" name="lastName">
						</div>
					</div>
					
					<label>Billing Address Line 1 * </label> <input type="text" name="addressLine1"> 
					<label>Billing Address Line 2 * </label> <input type="text" name="addressLine2">

					<div class="row" id="cityPostcodeCounty">
						<div class="small-4 medium-4 large-4 columns">
							<label>City/Town * </label> <input type="text" name="city">
						</div>
						<div class="small-4 medium-4 large-4 columns">
							<label>County * </label> <input type="text" name="county">
						</div>
						<div class="small-4 medium-4 large-4 columns">
							<label>Postcode * </label> <input type="text" name="postcode">
						</div>


					</div>
				</div>

			</div>

			<div class="row" id="paymentDetails">

				<div class="small-12 medium-6 large-6 columns" id="cardDetails">
				<h3>Payment Details</h3>
					<br>
					<h6>Please enter your credit/debit card details.</h6>
					<div class="row" id="cardNumber">
						<label>Card Number * </label> <input type="number"
							name="cardNumber" required placeholder="Enter your card number">
					</div>
					<div class="row" id="cardName">
						<label>Name on Card * </label> <input type="text"
							name="cardName" required
							placeholder="Enter your name as displayed on the card">
					</div>
					<div class="row" id="cardTypeExpiryCVC">
						<div class="small-3 columns" id="cardType">
							<label>Card Type * </label> <select name="cardType">
								<option value="visa">Visa</option>
								<option value="visaDebit">Visa Debit</option>
								<option value="mastercard">Mastercard</option>
								<option value="mastercardDebit">Debit Mastercard</option>
							</select>
						</div>
						<div class="small-3 columns" id="cardExpiryMonth">
							<label>Expiry Month * </label> <select name="exMonth"
								title="select a month" required>
								<option value="0">Enter month</option>
								<option value="01">January</option>
								<option value="02">February</option>
								<option value="03">March</option>
								<option value="04">April</option>
								<option value="05">May</option>
								<option value="06">June</option>
								<option value="07">July</option>
								<option value="08">August</option>
								<option value="09">September</option>
								<option value="10">October</option>
								<option value="11">November</option>
								<option value="12">December</option>
							</select>
						</div>
						<div class="small-3 columns" id="cardExpiryDay">
							<label>Expiry Year * </label> <select name="exYear"
								title="select a year" required>
								<option value="0">Enter year</option>
								<option value="2017">2017</option>
								<option value="2018">2018</option>
								<option value="2019">2019</option>
								<option value="2020">2020</option>
								<option value="2021">2021</option>
								<option value="2022">2022</option>
								<option value="2023">2023</option>
								<option value="2024">2024</option>
								<option value="2025">2025</option>
								<option value="2026">2026</option>
								<option value="2027">2027</option>
								<option value="2028">2028</option>
								<option value="2029">2029</option>
								<option value="2030">2030</option>
								<option value="2031">2031</option>
							</select>
						</div>
						<div class="small-3 columns" id="cardType">
							<label>CVC (eg 123)*</label><input type="password"
								name="cardCVC" required placeholder="Enter your 3 digit CVC"
								pattern="[0-9]{3}">
						</div>

						</div>
					</div>
					
					<div class="small-12 medium-6 large-6 columns" id="orderSummary">
					<h3>Order Summary</h3>
					<p></p>

					<div class="row">
						<div class="small-3 columns">
							<label for="middle-label" class="middle">Cart Total</label>
						</div>
						<div class="small-3 columns">

							<label for="middle-label" class="middle" id="cart_total_label">�<%=formatter.format(orderTotal)%>
							</label>
						</div>

					</div>

					<div class="row">
						<div class="small-3 columns">
							<label for="middle-label" class="middle">VAT </label>
						</div>
						<div class="small-3 columns">
							<label for="middle-label" class="middle">�<%=formatter.format(orderTotal*0.20)%></label>
						</div>

					</div>

					<div class="row">
						<div class="small-3 columns">
							<label for="middle-label" class="middle">Order Total </label>
						</div>
						<div class="small-3 columns">

							<label for="middle-label" class="middle" id="order_total_label">�<%=formatter.format(orderTotal)%>
							</label>
						</div>

					</div>


					<input type="hidden" name="order_total" value="<%=orderTotal%>" />
					<input type="submit" class="button large expanded"
						value="Confirm Payment" />

				</div>
					</div>
		</form>
		<%@include file="custom_footer.jsp"%>
	</div>
	<script>
		$(document).foundation();
	</script>
	<%@include file="script_files.jsp"%>

</body>
</html>


