<!doctype html>
<%@page import="com.qa.models.Book"%>
<%@page import="com.qa.models.Author"%>
<%@page import="java.io.File"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<html class="no-js" lang="en">
<head>
<%!Book book;%>

<%
	book = (Book) request.getAttribute("book");
%>

<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | <%=book.getTitle()%></title>
<%@include file="style_files.jsp"%>
<link rel="stylesheet" href="css/app.css">


</head>
<body class="main">
	<%
		NumberFormat formatter = new DecimalFormat("#0.00");
	%>
	<%@include file="navigation.jsp"%>
	<div class="whitebg">

		<br>
		<!-- You can now combine a row and column if you just need a 12 column row -->
		<div class="row columns">
			<nav aria-label="You are here:" role="navigation">
				<ul class="breadcrumbs">

					<li><a href="/">Home</a></li>
					<li><span class="show-for-sr">Current: </span> Book Details</li>
				</ul>
			</nav>
		</div>

		<div class="row columns">
			<h3>
				<strong><%=book.getTitle()%></strong>
			</h3>
		</div>
		<br />
		<div class="row">

			<div class="small-12 medium-4 columns">


				<%
					File f = new File("src/main/resources/static/" + book.getBookImage());

					File folder = new File("src/main/resources/static/images");
					File[] listOfFiles = folder.listFiles();

					for (int i = 0; i < listOfFiles.length; i++) {

						if (f.getPath().equals(listOfFiles[i].getPath())) {
							System.out.println("thinks pic exists");
				%>
				<img class="thumbnail" src="<%=book.getBookImage()%>" height=300px
					width=450px>
				<%
					break;
						}

						if (i == listOfFiles.length - 1) {
							System.out.println("thinks pic doesn't exist");
				%><img
					class="thumbnail" src="images/PlaceholderBook.png" height=300px
					width=450px>
				<%
					}
					}
				%>
				<div class="row">
					<div class="small-6 columns bookInfoNum">
						<strong> Price: </strong><br> �<%=formatter.format(book.getPrice())%>
					</div>
					<div class="small-6 columns bookInfoNum">
						<strong> Published On: </strong><br>
						<%=book.getPublishedDate()%>
					</div>
				</div>
				<div class="row">
					<div class="small-6 columns bookInfoNum">
						<strong> eBook ISBN: </strong><br>
						<%=book.geteBookISBN()%>
					</div>
					<div class="small-6 columns bookInfoNum">
						<strong> Print book ISBN: </strong><br>
						<%=book.getPaperISBN()%>
					</div>

				</div>

			</div>

			<div class="small-12 medium-4 columns">
				<h3>
					<Strong>Description</Strong>
				</h3>
				<p>
				<p><%=book.getDescription().replace("?","&#39;")%></p>

				<a href="/addToCart?bookId=<%=book.getBookId()%>"
					class="button large expanded">Add to Cart</a>


			</div>

			<div class="small-12 medium-4 columns">
				<h3>
					<strong>Authors</strong>
				</h3>
				<p>
					<%
						for (Author a : book.getAuthors()) {
					%>
				<div>
					<%=a.getAuthorName()%>
				</div>
				<%
					}
				%></p>
				<br />
				<h3>
					<strong>Contents</strong>
				</h3>
				<p><%=book.getTableOfContents()%>
			
			</div>
		</div>

		
		

		<%@include file="custom_footer.jsp"%>
	</div>
	<%@include file="script_files.jsp"%>
</body>
</html>