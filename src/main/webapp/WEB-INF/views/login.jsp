<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Bookspace | Login </title>
    <%@include file="style_files.jsp" %>
  </head>
  <body class="main">
    
<%@include file="navigation.jsp" %>
    <div class="whitebg">
    <br>
<%
						if (c == null) {
					%>
    <div >
      <div class="row column">
        
        
        <div class="medium-6">
           <h4> Please enter your account details to log in</h4>
                
               <form action="loginProcess" method="post"> 
              
               <label>Email <span style="color:red">*</span> </label>
                <input type="email" placeholder="Enter Email" name="email" id="email" required/> 
                <label>Password <span style="color:red">*</span> </label>
				 <input type="password" placeholder="Enter Password" name="password" id="password" required/>
            	<input type="submit" class="button" value="Login">
              
              </form>
            </div>
      
      <div class="medium-6">
           <br>
           <h4> New Customer?  </h4> <a href="/register" class="button">Register</a>
         
            </div>
      
       
      </div>
    </div>
    <%
 	} else if (c != null) {
 %>
   <p>You are already logged in! To log in to a different account, please <a href="/logout">log out</a> first</p>
   <% } %>
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="js/elsevier.js"></script>
    <script>
      $(document).foundation();
    </script>
    
    <%@include file="script_files.jsp" %>
    <%@include file="custom_footer.jsp"%>
    </div>
  </body>
</html>


    