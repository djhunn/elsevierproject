<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.qa.models.Customer"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Test</title>
</head>
<body>

	 <!-- Start Top Bar -->
    <div class="top-bar">
      <div class="top-bar-left">
        <ul class="menu">
          <li class="menu-text" style="color:red">Online Shopping</li>
          <li><a href="/">Home</a></li>
          
        </ul>
      </div>
      <div class="top-bar-right">
        
             <ul class="dropdown menu data-dropdown-menu">
            <li id="cart_items"></li>
            <li class="has-submenu">
              <a href="/viewCart"> <img src="images/cart.jpg" width="50" height="50"/></a>
              <ul class="submenu menu vertical data-submenu">
                <li><a href="/viewCart"><img src="images/cart.jpg" width="50" height="50"/> View Cart </a></li>
                <li><a href="/register">Register</a> | <a href="/login">Login</a></li>
              </ul>
            </li>
            <li><a href="/aboutUs">About Us</a></li>
            <li><a href="/contact">Contact</a></li>
          </ul>
          
      </div>
    </div>
    <!-- End Top Bar -->


<%Customer c;%>


<%c = (Customer) session.getAttribute("logged_in_customer");%>

<% if (c != null) { %>
You are logged in.<br>
<% } %>
<br>
<% if (c == null) { %>
You are not logged in.
<% } %>
<br>It should say if you're logged in.


<a href="/logout">Logout</a>



<div class="row" id="cardTypeExpiryCVC">
						<div class="small-3" id="cardType">
							<label>Card Type * </label> <select name="cardType">
								<option value="visa">Visa</option>
								
							</select>
						</div>
						<div class="small-3" id="cardExpiryMonth">
							<label>Expiry Date (Year) * </label> <select name="exMonth"
								title="select a month" required>
								<option value="0">Enter month</option>
								
							</select>
						</div>
						<div class="small-3" id="cardExpiryDay">
							<label>Expiry Date (Month) * </label> <select name="exYear"
								title="select a year" required>
								<option value="0">Enter year</option>
								
							</select>
						</div>
						<div class="small-3" id="cardType">
							<label>Security Code (CVC) </label><input type="password"
								name="cardCVC" required placeholder="Enter your CVC"
								pattern="[0-9]{3}">
						</div>


</div>


</body>
</html>