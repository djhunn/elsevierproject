
<%@page import="com.qa.models.Customer"%>

<%
	Customer c = null;
	if (request.isRequestedSessionIdValid()) {
		c = (Customer) session.getAttribute("logged_in_customer");
	}
%>

<!-- Start Top Bar -->

<header class="sticky" id="custom-navbar">

	<!-- Changes what menu looks like when screen is small -->
	<div class="title-bar" data-responsive-toggle="example-menu"
		data-hide-for="medium">
		<div class="title-bar-title">
			<a href="/"><img src="/images/newbookspacelogo.png"
				style="height: 40px; width: 200px;" alt="Bookspace logo"></a>
		</div>
		<button class="menu-icon right" type="button" data-toggle></button>
	</div>


	<div class="top-bar" id="example-menu">

		<!-- Displays logo in top left -->
		<div class="top-bar-left">
			<ul class="menu">
				<li class="menu-text"><a href="/"><img
						src="/images/newbookspacelogo.png"
						style="height: 40px; width: 200px;"></a></li>
			</ul>
		</div>

		<!-- Displays menu in top right -->
		<div class="top-bar-right">
			<ul class="dropdown menu sub"
				data-responsive-menu="drilldown medium-dropdown">
				<li><a href="/">Home</a></li>
				<li><a href="/bookPortal">Book Portal</a></li>
				<li><a href="/aboutUs">About Us</a></li>
				<li><a href="/contact">Contact</a></li>
				<%
					if (c == null) {
				%><li><a href="/login">My Account</a> <!-- If logged in, the only change we want is to the options under "My Account": we want
a log out option, order history, view account details, and modify account details -->

					<ul class="menu vertical">
						<li><a href="/login">Sign In</a></li>
						<li><a href="/register">Create Account</a></li>
					</ul> <%
 	} else if (c != null) {
 %>
				<li><a>My Account</a>
					<ul class="menu vertical">
						<li><a href="/logout">Log Out</a></li>
						<li><a href="/myOrders">Order History</a></li>
						<li><a href="/profile">Account Details</a></li>
					</ul> <%
 	}
 %></li>
				<li><a href="/viewCart"> <img
						src="images/shopping_cart_white.png" width="50" height="50" alt="Shopping cart"/></a></li>
				<li>
					<div class="dropdown">
						<button onclick="dropdownToggle()" class="dropbtn">
							<a><img src="images/search-icon-white.png" width="37.5"
								height="37.5" alt="Magnifying glass"/></a>
						</button>

					</div>
				</li>
			</ul>
		</div>
	</div>
	<div id="myDropdown" class="dropdown-content">
		<%
			if (c == null) {
		%>
		<form action="findBooks" method="post">
			<div class="search-wrapper row" id="search-box-wrapper">
				<div class="large-11 columns">
					<input id="search-input" name="search-input" type="text" placeholder="Search...">
				</div>
				<div class="large-1 columns" id="search-icon-wrapper">
					<input type="image" id="img-dropdown-search"
						src="images/search-icon-white.png" alt="Search" height=30px width=30px />
				</div>
			</div>
		</form>
		<%
			} else {
		%>
		<form action="findBooks2" method="post">
			<div class="search-wrapper row" id="search-box-wrapper-loggedin">
				<div class="large-11 columns">
					<input id="search-input-2" name="search-input-2" type="text" placeholder="Search...">
				</div>
				<div class="large-1 columns" id="search-icon-wrapper-loggedin">
					<input type="image" id="img-dropdown-search-loggedin"
						src="images/search-icon-white.png" alt="Search" height=30px width=30px />
				</div>
			</div>
		</form>
		<%
			}
		%>
	</div>
	<script>
		function dropdownToggle() {
			document.getElementById("myDropdown").classList.toggle("show");
		}
	</script>
</header>

<!-- End Top Bar -->