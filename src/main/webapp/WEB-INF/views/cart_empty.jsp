<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Your Cart</title>
<%@include file="style_files.jsp"%>
</head>
<body class="main">
	<%@include file="navigation.jsp"%>
	<div class="whitebg">
		<div>
			<div class="row column text-center">
				<br>
				<h1>Your cart is empty</h1>


				<br> <a href="/bookPortal" class="button large"> Continue
					shopping </a>

			</div>
		</div><%@include file="custom_footer.jsp"%>

	</div>
	<%@include file="script_files.jsp"%>
</body>
</html>


