<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Register</title>
<%@include file="style_files.jsp"%>
</head>
<body class="main">

	<%@include file="navigation.jsp"%>
	<div class="whitebg">
		<div class="row column text-center">
		<br>
			<h1>Registration failed</h1>
<br>
			<h4>
				One of the following issues caused your registration to fail:<br>
			</h4>

			<h4>
				-There is already an account associated with this email address. <br>
				-You did not enter any information into the required fields.
			</h4>
<br>
			<a href="/register" class="button large"> Click here to try again
			</a>

		</div>
		<%@include file="custom_footer.jsp"%>
	</div>


	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="js/elsevier.js"></script>
	<script>
		$(document).foundation();
	</script>
	<%@include file="script_files.jsp"%>
	</div>
</body>
</html>


