<!doctype html>
<%@page import="java.util.Map"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.qa.models.Book"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.io.File"%>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Your Cart</title>
<%@include file="style_files.jsp"%>
<%
	NumberFormat formatter = new DecimalFormat("#0.00");
%>

</head>
<body class="main">

	<%!ArrayList<Book> books;

	Map<Integer, Integer> bookCounts;%>


	<%
		System.out.println(session.getAttribute("cart_items"));
		books = (ArrayList<Book>) session.getAttribute("filtered_books");

		bookCounts = (Map<Integer, Integer>) session.getAttribute("book_counts");

		double cartTotal = 0.0;

		double orderTotal = 0.0;

		double totalPrice = 0.0;
	%>



	<%@include file="navigation.jsp"%>
	<div class="whitebg">

		<br>
		<!-- You can now combine a row and column if you just need a 12 column row -->
		<div class="row columns">
			<nav aria-label="You are here:" role="navigation">
				<ul class="breadcrumbs">

					<li><a href="/">Home</a></li>
					<li><span class="show-for-sr">Current: </span> Cart Details</li>
				</ul>
			</nav>
		</div>

		<div class="row">
			<div class="medium-6 columns">
				<%
					int i = 0;
					for (Book book : books) {

						int quantity = bookCounts.get(book.getBookId());
						double price = book.getPrice();
						totalPrice = book.getPrice() * quantity;
						cartTotal = cartTotal + book.getPrice() * quantity;
						System.out.println("Cart Total " + cartTotal);
				%>

				<%
					File f = new File("src/main/resources/static/" + book.getBookImage());

						File folder = new File("src/main/resources/static/images");
						File[] listOfFiles = folder.listFiles();

						for (int k = 0; k < listOfFiles.length; k++) {

							if (f.getPath().equals(listOfFiles[k].getPath())) {
								System.out.println("thinks pic exists");
				%>
				<a href="/bookDetails?bookId=<%=book.getBookId()%>"><img
					class="thumbnail" src="<%=book.getBookImage()%>" height=200px
					width=300px alt="Book cover"></a>
				<%
					break;
							}

							if (k == listOfFiles.length - 1) {
								System.out.println("thinks pic doesn't exist");
				%><a
					href="/bookDetails?bookId=<%=book.getBookId()%>"><img
					class="thumbnail" src="images/PlaceholderBook.png" height=200px
					width=300px alt="Placeholder cover"></a>
				<%
					}
						}
				%>

				<div class="row small-up-2">
					<div class="column">
						<strong>Price: </strong>
						<div class="row columns">
							<label id="price_label<%=i%>">�<%=formatter.format(totalPrice / quantity)%></label>
						</div>
					</div>
					<div class="column">
						<strong>Published On:</strong>
						<div class="row columns">
							<%=book.getPublishedDate()%></div>
					</div>
				</div>
				<div class="row small-up-2">

					<div class="column">
						<strong>eBook ISBN: </strong>
						<div class="row columns">
							<%=book.geteBookISBN()%></div>
					</div>
					<div class="column">
						<strong>Print book ISBN: </strong>
						<div class="row columns">
							<%=book.getPaperISBN()%></div>
					</div>

					<div class="column">
						<form name="f1" action="/updateCart" method="get">


							<input type="hidden" name="price" value="<%=price%>" /> <input
								type="hidden" name="bookId" value="<%=book.getBookId()%>" /> <input
								type="hidden" name="cart_total" value="<%=cartTotal%>" /> <input
								type="hidden" name="cart_total" value="<%=price%>" /> Quantity <input
								type="number" min="1" name="quantity" value="<%=quantity%>"
								oninput="setQuantity(this.form)" /> <input type="hidden"
								name="userQuantity" value="" /> <input type="submit"
								class="update cart button" value="Update Cart">
						</form>
					</div>


				</div>

				<div class="row small-up-4">

					<div class="column">
						<a href="/removeFromCart?bookId=<%=book.getBookId()%>">
							Remove </a>
					</div>

				</div>

				<hr>
				<%
					i++;
					}
				%>

			</div>
			<div class="medium-6 large-5 columns">
				<h3>Order Summary</h3>
				<p></p>

				<div class="row">
					<div class="small-3 columns">
						<label for="middle-label" class="middle">Cart Total</label>
					</div>
					<div class="small-3 columns">
						<input type="hidden" name="order_total" id="cart_total"
							value="<%=cartTotal%>" /> <label for="middle-label"
							class="middle" id="cart_total_label">�<%=formatter.format(cartTotal)%></label>
					</div>

				</div>


				<div class="row">
					<div class="small-3 columns">
						<label for="middle-label" class="middle">VAT (inc.) </label>
					</div>
					<div class="small-3 columns">
						<label for="middle-label" class="middle">�<%=formatter.format(cartTotal * 0.2)%></label>
					</div>

				</div>

				<div class="row">
					<div class="small-3 columns">
						<label for="middle-label" class="middle">Order Total </label>
					</div>
					<div class="small-3 columns">
						<input type="hidden" name="order_total" id="order_total"
							value="<%=cartTotal%>" /> <label for="middle-label"
							class="middle" id="order_total_label">�<%=formatter.format(cartTotal)%></label>
					</div>

				</div>

				<form action="/checkout" method="post" id="checkout_form">
					<input type="hidden" name="order_total" value="<%=cartTotal%>" />
					<input type="submit" class="button large expanded"
						value="Proceed to Checkout" />
				</form>
			</div>
		</div><%@include file="custom_footer.jsp"%></div>
	<%@include file="script_files.jsp"%>
</body>
</html>


