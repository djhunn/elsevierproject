
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Contact Us</title>
<%@include file="style_files.jsp"%>


</head>

<body class="main">

	<%@include file="navigation.jsp"%>
	<div class="whitebg">
		<div class="row columns">
			<br>
			<h1>Contact Us</h1>

			<p>We love to hear from our Customers! If you have any questions,
				please feel free to contact us.</p>

			<h4>Phone Number</h4>
			<p>
				<strong>0800-000-0000</strong>
			</p>

			<p>
				<em> Please note that our standard phone hours are <br>
					08:30 - 17:30 on Weekdays<br> 09:30 - 16:30 on Saturdays<br>
					10:00 - 15:00 on Sundays
				</em>
			</p>

			<h4>Email</h4>
			<p>
				Alternatively, drop us an email at <br> <strong>
					bookspacecompany@gmail.com </strong> <br> and we will try to get back
				to you as soon as possible
			</p>

			<h4>Contact Us Form</h4>

			<p>At Bookspace, we are always looking to expand our selection of
				available books. If you know of a proper good book that you'd love
				to see in our store, then drop us a message using the form below and
				we try our best to stock it in the future.</p>



			<div class="row">

				<form action="requests" method="post" name="bookRequests">

					<div class="small-6 up columns end">
						Your Name <span style="color: red">*</span><br> <input
							id="name" type="text" size="20" name="Name"
							placeholder="Enter your name here" required> <br>

						Your Email <span style="color: red">*</span><br> <input
							id="email" type="text" size="20" name="Email"
							placeholder="Enter your email address here" required> <br>

						Book Name <span style="color: red">*</span><br> <input
							id="bookName" type="text" size="20" name="BookName"
							placeholder="Enter the book's name here" required> <br>

						Book Author <span style="color: red">*</span><br> <input
							id="bookAuthor" type="text" size="20" name="BookAuthor"
							placeholder="Enter the book's author here" required> <br>

						<input type="submit" value="Submit" class="button">
					</div>
				</form>
			</div>




		</div>
		<%@include file="custom_footer.jsp"%>
	</div>
	<%@include file="script_files.jsp"%>
</body>
</html>