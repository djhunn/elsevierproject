<html>
<head>
<title>Error</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Error</title>
<%@include file="style_files.jsp"%>
</head>
<body class="main">
	<%@include file="navigation.jsp"%>
	<div class="whitebg">
	<br>
		<p style="text-align:center">You're in a right proper mess, return <a href="/">home</a>?</p><br>
		<div>
			<img src="/images/errorpic.jpg" width=100% height=90% alt="Error page picture">
		</div>
		<%@include file="custom_footer.jsp"%>
	</div>
	<%@include file="script_files.jsp"%>
</body>
</html>