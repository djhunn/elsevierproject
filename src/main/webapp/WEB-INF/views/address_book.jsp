<!doctype html>
<%@page import="com.qa.models.Customer"%>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Bookspace | Address Book </title>
    <%@include file="style_files.jsp" %>
  </head>
  <body class="main">
  
   <%@include file="navigation.jsp" %>
    
       <div class="whitebg">
       <div class="row">
        
       
        <div class="large-6 columns">
           <h3> Billing Address </h3>
                
              <form action="/updateAddress" method="post">
               <label>Address Line 1 * </label>
               <input type="text" placeholder="Enter Address Line 1" name="addressLine1" id="addressLine1" required/> 
               <label>Address Line 2 * </label>
               <input type="text" placeholder="Enter Address Line 2" name="addressLine2" id="addressLine2"required/> 
               <label>City * </label>
                <input type="text" placeholder="Enter City" name="city" id="city"required/> 
                  <label>Postcode * </label>
                <input type="text" placeholder="Enter Postcode" name="postcode" id="postcode"required/> 
                
                
                  <label>State/ County * </label>
                <input type="text" placeholder="Enter State/ County" name="state" id="state"required/> 
                
                
              <!--     <label>Country * </label>
                <input type="text" placeholder="Enter Country" name="country" id="county"required/>  -->
                
                
                  <label>Phone Number * </label>
                <input type="number" placeholder="Enter Phone number" name="phone" id="phone"required/> 
                
                 
                <input type="hidden" name="addressType" id="addressType" value="billing"required/> 
                
                
                <input type="submit" class="button create account" value="Update Billing Address">
               </form>
            </div>
            
          
               <div class="medium-6 columns">
                <h3> Shipping Address </h3>
                
                <input type="checkbox" class="ba" checked="checked" /> Different from billing address
                
              <form action="/updateAddress" method="post">
               <label>Address Line 1 * </label>
               <input type="text" placeholder="Enter Address Line 1" name="addressLine1" id="addressLine1" class="tex"required/> 
               <label>Address Line2 * </label>
               <input type="text" placeholder="Enter Address Line 2" name="addressLine2" id="addressLine2" class="tex"required/> 
               <label>City * </label>
                <input type="text" placeholder="Enter City" name="city" id="city" class="tex"required/> 
                  <label>Postcode * </label>
                <input type="text" placeholder="Enter Postcode" name="postcode" id="postcode" class="tex"required/> 
                
                
                  <label>State/ County * </label>
                <input type="text" placeholder="Enter State/ County" name="state" id="state" class="tex"required/> 
                
                
                <!--   <label>Country * </label>
                <input type="text" placeholder="Enter Country" name="country" id="county" class="tex"required/>  -->
                
                
                  <label>Phone Number * </label>
                <input type="number" placeholder="Enter Phone number" name="phone" id="phone" class="tex"required/> 
                
                 
                <input type="hidden" name="addressType" id="addressType" value="shipping"/> 
                
                
                <input type="submit" class="button create account" value="Update Shipping Address">
               </form>
            </div>
             
     
      </div>
      
  
       
       
  <!--   </div> -->
    
    
         
       
    </div>
    </div>
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="js/elsevier2.js"></script>
    <script>
      $(document).foundation();
    </script>
     <%@include file="script_files.jsp" %>
  </body>
</html>


    