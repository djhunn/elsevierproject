<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Register</title>
<%@include file="style_files.jsp"%>
</head>
<body class="main">

	<%@include file="navigation.jsp"%>
	<div class="whitebg">
		<div class="row column text-center">
		<br>
			<h1>Registration Success</h1>
<br>
			<a href="/login" class="button large"> Click here to log in </a>

		</div>
		<%@include file="custom_footer.jsp"%>
	</div>


	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="js/elsevier.js"></script>
	<script>
		$(document).foundation();
	</script>
	<%@include file="script_files.jsp"%>
</body>
</html>


