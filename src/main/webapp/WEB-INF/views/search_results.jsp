<!doctype html>
<%@page import="com.qa.models.Book"%>
<%@page import="com.qa.models.Author"%>
<%@page import="java.util.List"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.io.File" %>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Search Results</title>
<%@include file="style_files.jsp"%>

</head>
<body class="main">

	<%
		NumberFormat formatter = new DecimalFormat("#0.00");
	%>

	<%!Book book;%>


	<%
		book = (Book) request.getAttribute("book");
	%>

	<%!List<Book> books;%>


	<%@include file="navigation.jsp"%>
	<div class="whitebg">
		<br> <br>

		<%
			books = (List<Book>) request.getAttribute("search_books");
		%>
		<%
			if (books.size() == 0) {
		%>
		<div class="row column">
			<h3>We don't stock any books that match your search!
				If you would like to request a specific book please fill out the
				form below:</h3>
		</div>
		<div class="row">

			<form action="requests" method="post" name="bookRequests">

				<div class="small-6 up columns end">
					Your Name <span style="color:red">*</span><br> <input id="name" type="text" size="20"
						name="Name" placeholder="Enter your name here" required>
						<br>
						
					Your Email <span style="color:red">*</span><br> <input
						id="email" type="text" size="20" name="Email" placeholder="Enter your email address here" required>
						<br>

					Book Name <span style="color:red">*</span><br> <input id="bookName" type="text" size="20"
						name="BookName" placeholder="Enter the book's name here" required>
						<br>
					
					Book Author <span style="color:red">*</span><br> <input
						id="bookAuthor" type="text" size="20" name="BookAuthor" placeholder="Enter the book's author here" required>
						<br>

					<input type="submit" value="Submit" class="button">
				</div>
			</form>
		</div>
		<div class="small-0 medium-3 large-4 columns"></div>

		<%
			} else {
		%>
		<div align="center">
			<h3>
				We have found
				<%
				if (books.size() == 1) {
			%>
				1 book matching your search
				<%
				} else {
			%>
				<%=books.size()%>
				books matching your search, happy days!
				<%
					}
				%>
			</h3>
		</div>
		<%
			for (Book b : books) {
		%>

		<div>
			<hr>
			<div class="small-12 medium-4 large-3 columns" align="center">
				<% File f = new File("src/main/resources/static/"+b.getBookImage());
								
				File folder = new File("src/main/resources/static/images");
				File[] listOfFiles = folder.listFiles();
				
				for(int i=0;i<listOfFiles.length;i++){
					
					if (f.getPath().equals(listOfFiles[i].getPath())) {
					System.out.println("thinks pic exists");%>
			    	  <img
						class="thumbnail" src="<%=b.getBookImage()%>" height=200px
						width=300px alt="Book cover"><%
						break;}
					
			      if (i==listOfFiles.length-1) {System.out.println("thinks pic doesn't exist");%><img
				class="thumbnail" src="images/PlaceholderBook.png" height=200px
				width=300px alt="Placeholder cover"><%
			      } 
				} %>
			</div>
			<div class="small-12 medium-7 large-6 columns">
				<p>
					<strong><%=b.getTitle()%></strong>
				</p>
				<p>
					<strong>Price:</strong> �<%=formatter.format(b.getPrice())%></p>
				<p>
					<strong>Author: </strong>
					<%
						if (b.getAuthors().size() > 0) {

									for (int i = 0; i < b.getAuthors().size() - 1; i++) {
					%>
					<%=b.getAuthors().get(i).getAuthorName()%>,
					<%
						}
					%>
					<%=b.getAuthors().get(b.getAuthors().size() - 1).getAuthorName()%>.
					<%
						}
					%>
				
				<p>
					<strong>Publication Date:</strong>
					<%=b.getPublishedDate()%></p>
				<p>
					<strong>Brief description:</strong>
					<%=b.getDescription()%>
				</p>
				<p>
					<a href="/bookDetails?bookId=<%=b.getBookId()%>"
						class="button expanded">View book details</a>
				</p>
			</div>
			<div class="small-0 medium-1 large-3 columns"></div>
		</div>
		<%
			}
		%>
		<%
			}
		%>
		<%@include file="custom_footer.jsp"%>


		<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
		<script src="js/elsevier.js"></script>
		<script>
			$(document).foundation();
		</script>
		<%@include file="script_files.jsp"%>
	</div>
</body>
</html>