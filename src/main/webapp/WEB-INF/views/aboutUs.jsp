<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | About Us</title>

<%@include file="style_files.jsp"%>

</head>
<body class="main">

	<%@include file="navigation.jsp"%>

	<div class="whitebg">
		<div class="row column text-center">
		<br />
			<h1>About Bookspace</h1>
		</div>

	<div class="row column">
		<div id="tabs">
			<ul>
				<li><a href="#tabs-1">Our History</a></li>
				<li><a href="#tabs-2">Our Mission</a></li>
				<li><a href="#tabs-3">Our Team</a></li>
			</ul>
			<div id="tabs-1">
				<p>
					Set up in late 2017, Bookspace is a new type of bookshop, with an
					intense focus on one thing alone: <em>Proper. Good. Books.</em>
				</p>
				<p>Our staff have worked tirelessly to ensure that this website
					makes it easy and enjoyable for you to peruse our collection of
					proper good books, find your favourites, and make orders.</p>
					
					<p>Our history may be short, but if you were to <a href="/register">register</a> today then we believe we could come together to expand Bookspace and, in turn, our minds.</p>
			</div>
			<div id="tabs-2">
				<p>Here at Bookspace, we have made it our sole purpose in life to bring the overwhelming joy of proper good books to everyone in the U.K., and eventually the entire world.</p><br>
								
				<p>
				
				</p>
			</div>
			<div id="tabs-3">
				<div class="row">
					<div class="small-12 medium-2 large-2 large-offset-1 columns aboutProfile">
						<br><p align="center"><img src="./images/danny.png" alt="Danny"><h5><strong>Danny Hunn</strong></h5>
						<p>After taking a football to the knee, Danny put down his shin pads and swore a solemn vow to defend the good word of books all over the globe.</p>
					</div>
					<div class="small-12 medium-2 large-2 columns aboutProfile">
						<br><p align="center"><img src="./images/vishal.png" alt="Vishal"><h5><strong>Vishal Patel</strong></h5>
						<p>Despite his brooding, sullen nature, Vish is surprisingly passionate; the one thing he cares about more than books are the people who write them. </p>
					</div>
					<div class="small-12 medium-2 large-2 columns aboutProfile">
						<br><p align="center"><img src="./images/tessa.png" alt="Tessa"><h5><strong>Tessa Hoad</strong></h5>
						<p>Extremely diligent and attentive, the only thing Tessa picks up more than the rest of the team's slack is a good, proper book.</p>
					
					</div>
					<div class="small-12 medium-2 large-2 columns aboutProfile charlie">
						<br><p align="center"><img src="./images/charlie.png" alt="Charlie"/><h5><strong>Charlie Williams</strong></h5>
						<p>Charming and disarming, Charlie will use any means necessary to promote books and reading to everyone. Illiteracy be damned.</p>
					</div>
					
					<div class="small-12 medium-2 large-2 columns aboutProfile end">
						<br><p align="center"><img src="./images/anthony.png" alt="Anthony"/><h5><strong>Anthony Landau</strong></h5>
						<p> Ever professional, ever vigilant. Anthony has served the order of books for his entire life. He has never touched a kindle.</p>
					</div>
					
				</div>
			</div>

		</div>
		
	</div>
	<%@include file="custom_footer.jsp"%>
	</div>
	<%@include file="script_files.jsp"%>
	<script src="/js/jquery-ui.js"></script>

	<script>
		$(function() {
			$("#tabs").tabs();
		});
	</script>
</body>
</html>