<!doctype html>
<%@page import="com.qa.models.Customer"%>
<%@page import="com.qa.models.Order"%>
<%@page import="com.qa.models.Book"%>
<%@page import="com.qa.models.Address"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.io.File"%>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Order History</title>
<%@include file="style_files.jsp"%>
</head>
<body class="main">

	<%
		NumberFormat formatter = new DecimalFormat("#0.00");
	%>



	<%
		List<Order> orderList;
	%>

	<%
		orderList = (List<Order>) session.getAttribute("customer_orders");
	%>
	<%@include file="navigation.jsp"%>
	<div class="whitebg">
		<div style="padding: 30px;">
			<div class="row large-12 medium-12 small-12 columns">
				<h3><%=c.getFirstName()%>'s Orders
				</h3>
			</div>
			<%
				for (Order o : orderList) {
					
			%>
			<hr />
			<div class="row">
				<div class="large-8 medium-8 small-12 columns">
					<div class="row">
						<h4>
							Order #<%=o.getOrder_id()%></h4>
					</div>
					<div class="row">
						<div class="row">
							<div class="small-6 medium-2 large-2 columns">Order date:</div>
							<div class="small-6 medium-10 large-10 columns">
								<%=o.getDate()%>
							</div>
						</div>
						<br>
						<div class="row">

							<div class="small-6 medium-2 large-2 columns">Order total:</div>
							<div class="small-6 medium-10 large-10 columns">
								�<%=formatter.format(o.getTotal())%>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="small-12 medium-2 large-2 columns">Items:</div>
							<div class="small-12 medium-7 large-7 end columns">
								<table class="book-table">
									<tr class="book-table-header">
										<td>Title</td>
										<td class="number-column">Quantity</td>
									</tr>

									<%
										for (Map.Entry<Book,Integer> b : o.getBookQuantity().entrySet()) {
									%>

									<tr>
										<td><a
											href="/bookDetails?bookId=<%=b.getKey().getBookId()%>"><%=b.getKey().getTitle()%></a></td>
										<td class="number-column"><%=b.getValue()%></td>
									</tr>

									<%
										}
									%>
								</table>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="small-6 medium-2 large-2 columns">Shipping
								Address:</div>
							<div class="small-6 medium-4 large-4 columns">
								<div><%=o.getShippingAddress().getAddressLine1()%></div>
								<div><%=o.getShippingAddress().getAddressLine2()%></div>
								<div><%=o.getShippingAddress().getCity()%></div>
								<div><%=o.getShippingAddress().getCounty()%></div>
								<div><%=o.getShippingAddress().getPostcode()%></div>
								<br>
							</div>
							<div class="small-6 medium-2 large-2 columns">Billing
								Address:</div>
							<div class="small-6 medium-4 large-4 columns">
								<div><%=o.getBillingAddress().getAddressLine1()%></div>
								<div><%=o.getBillingAddress().getAddressLine2()%></div>
								<div><%=o.getBillingAddress().getCity()%></div>
								<div><%=o.getBillingAddress().getCounty()%></div>
								<div><%=o.getBillingAddress().getPostcode()%></div>
								<br>
							</div>
						</div>
					</div>
				</div>
				
				<div class="large-4 medium-4 small-12 columns">
					<div class="row small-up-2 large-up-2">
						<%
							for (Book b : o.getBookQuantity().keySet()) {
						%>
						<div class="column">
							<% File f = new File("src/main/resources/static/"+b.getBookImage());
								
				File folder = new File("src/main/resources/static/images");
				File[] listOfFiles = folder.listFiles();
				
				for(int i=0;i<listOfFiles.length;i++){
					
					if (f.getPath().equals(listOfFiles[i].getPath())) { %>
							<a href="/bookDetails?bookId=<%=b.getBookId()%>"><img
								class="thumbnail" src="<%=b.getBookImage()%>" height=300px
								width=450px alt="Book cover"></a>
							<%
						break;}
						 if (i==listOfFiles.length-1) {%><a
								href="/bookDetails?bookId=<%=b.getBookId()%>"><img
								class="thumbnail" src="images/PlaceholderBook.png" height=300px
								width=450px alt="Placeholder cover"></a>
							<%
			      } 
				} %>
						</div>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<%
				}
			%>
		</div>
		<%@include file="custom_footer.jsp"%>
	</div>

	<%@include file="script_files.jsp"%>

</body>
</html>