<!doctype html>
<%@page import="com.qa.models.Customer"%>
<html class="no-js" lang="en">
  <head>
  
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Bookspace | Register </title>
  
    <%@include file="style_files.jsp" %>
    
  </head>
  <body class="main">
   
   <%@include file="navigation.jsp" %>
  
    <div class="whitebg">
    <br>
     <%if (c == null) {%>
    <div>
      <div class="row column">
        
        <form action="/registerProcess" method="post">
        <div class="medium-6">
           <h3>Create an account</h3>
                
                
               <label>First Name <span style="color:red">*</span></label>
               <input type="text" placeholder="Enter First Name" name="firstName" id="firstName" pattern="[A-Za-z\s-]{1,}" title="Input must be non-empty and only include letters/ hyphens" required/> 
               <label>Last Name <span style="color:red">*</span> </label>
               <input type="text" placeholder="Enter Last Name" name="lastName" id="lastName" pattern="[A-Za-z\s-]{1,}" title="Input must be non-empty and only include letters/ hyphens" required/> 
               <label>Email Address <span style="color:red">*</span> </label>
                <input type="email" placeholder="Enter Email" name="email" id="email" title= "Input must be a valid email address" required/> 
                <label>Password <span style="color:red">*</span> </label>
				 <input type="password" placeholder="Enter Password" name="password" id="password1" required/>
            	<label>Confirm Password <span style="color:red">*</span> </label>
				 <input type="password" placeholder="Confirm Password" name="passwordC" id="password2" required/>
            </div>
            
            <!-- Following script ensures that passwords match
            (unless javascript is turned off, in which case server-side validation is needed) -->
            <script>
           		password1.addEventListener('change', checkPasswordValidity, false);
           		password2.addEventListener('change', checkPasswordValidity, false);
   				function checkPasswordValidity() {
   					if (document.getElementById('password1').value != document.getElementById('password2').value) {
   					    document.getElementById('password2').setCustomValidity('Passwords must match.');
   					} else {
   					    document.getElementById('password2').setCustomValidity('');
   					}
				}
			</script>
            
            <div class="medium-6">
           
          	 <input type="checkbox" name="agreement" required> I have read and agree with the 
          	 <a href="/registered_user_agreement" target="_blank">Registered User Agreement</a>. 
         
            </div>
      		
      		<input type="submit" class="button create account" value="Create Account">
              
              </form>
    
      
      
      </div>
    </div>
    
<% } else if (c != null) {
 %>
   <p>You are already logged in! <a href="/logout">Log out</a> to register a new account.</p>
   <% } %>
 
   <%@include file="custom_footer.jsp"%>
   </div>

   
    <%@include file="script_files.jsp" %>
  </body>
</html>


    