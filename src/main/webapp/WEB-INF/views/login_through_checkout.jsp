<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Login</title>
<%@include file="style_files.jsp"%>
</head>
<body class="main">
	<%@include file="navigation.jsp"%>
	<div class="whitebg">


		<div class="row column text-center">


			<div class="medium-6 small-12 columns">
				<br>
				<h3>Please login using your stored credentials</h3>
				<br>
				<form action="/loginProcessThroughCheckout" method="post">
					<input type="email" placeholder="Enter email" name="email"
						id="email" required /> <input type="password"
						placeholder="Enter Password" name="password" id="password"
						required /> <input type="submit" class="button expanded"
						value="Submit">

				</form>
			</div>

		</div>


		<%@include file="script_files.jsp"%>
		<%@include file="custom_footer.jsp"%></div>
</body>
</html>


