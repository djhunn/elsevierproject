<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Registered User Agreement</title>
<%@include file="style_files.jsp"%>
</head>
<body class="main">

	<%@include file="navigation.jsp"%>
	<div class="whitebg">
		<div class="row column text-center">
		<br>
			<h3>Registered User Agreement</h3></br>
This Registered User Agreement ("Agreement") sets forth the terms and conditions governing the use of the Bookspace websites, online services and interactive applications (each, a "Service") by registered users. By becoming a registered user, completing the online registration process and checking the  box "I have read and agree to the Registered User Agreement" on the registration page, and using the Service, you agree to be bound by all of the terms and conditions of this Agreement.</br>
</br>
This Agreement is between you and Bookspace, who owns the respective Service. This Agreement expressly incorporates by reference and includes the respective Service's Terms and Conditions. In the event of any conflicts or inconsistencies between those terms and this Agreement, this Agreement will control.
</br></br>
Please carefully review this Agreement before ticking the relevant check box on the registration form. If you do not wish to accept this Agreement, do not proceed with the registration.</br>
</br></br><h4>Changes</h4>
Bookspace reserves the right to update, revise, supplement and otherwise modify this Agreement from time to time. Any such changes will be effective immediately and incorporated into this Agreement. Registered users are encouraged to review the most current version of the Agreement on a periodic basis  for changes. Your continued use of a Service following the posting of any changes constitutes your acceptance of those changes.
</br></br><h4>Password use and security</h4>
You must not reveal your password and must take reasonable steps to keep your password confidential and secure. You agree to immediately notify Bookspace if you become aware of or have reason to believe that there is any unauthorized use of your password or account or any other breach of security. Bookspace is in no way liable for any claims or losses related to the use or misuse of your password or account due to the activities of any third party outside of our control or due to your failure to maintain their confidentiality and security.  
</br></br><h4>Grant of license</h4>
As a registered user of a Service, Bookspace grants to you a non-transferable, non-exclusive and revocable license to use the Service according to the terms and conditions set forth in this Agreement. Except as expressly granted by this Agreement or otherwise by Bookspace or its licensors in writing,  you acquire no right, title or license in the Service or any data, content, application or materials accessed from or incorporated in the Service.
</br></br><h4>Term and termination</h4>
The license is effective until it expires, until Bookspace terminates it, or until you provide notice to Bookspace of your decision to terminate it. Your rights under this license will terminate automatically without notice to you if you fail to comply with any of the provisions of this Agreement. Bookspace  reserves the right to suspend, discontinue or change a Service, or its availability to you, at any time without notice. Upon termination of the license to a Service, you shall cease all use of the Service.
</br></br><h4>No assignment</h4>
THIS AGREEMENT IS PERSONAL TO YOU, AND YOU MAY NOT ASSIGN YOUR RIGHTS OR OBLIGATIONS TO ANYONE.</br>
</br></br><h4>No waiver</h4>
Neither failure nor delay on the part of Bookspace to exercise or enforce any right, remedy, power or privilege hereunder nor course of dealing between the parties shall operate as a waiver thereof, or of the exercise of any other right, remedy, power or privilege. No term of this Agreement shall be  deemed waived, and no breach consented to, unless such waiver or consent shall be in writing and signed by the party claimed to have waived or consented. No waiver of any rights or consent to any breaches shall constitute a waiver of any other rights or consent to any other breach.
</br></br><h4>Severability</h4>
If any provision in this Agreement is held invalid or unenforceable under applicable law, the remaining provisions shall continue in full force and effect.
</br></br><h4>Governing law and venue</h4>
This Agreement will be governed by and construed in accordance with the laws of the State of New York, USA, without regard to conflicts of law principles, except if you reside outside of the United States, then the laws of the country of the Bookspace regional office in the region where you reside.  The exclusive jurisdiction and venue with respect to any action or suit arising out of or pertaining to this Agreement shall be the courts of competent jurisdiction located in the State of New York, USA, except if you reside outside of the United States, then the courts located in the country of the Bookspace regional office in the region where you reside. This Agreement will not be governed by the United Nations Convention on Contracts for the International Sale of Goods.
</br></br>Last revised: 2 May 2013
		</div>
		<%@include file="custom_footer.jsp"%>
	</div>


	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="js/elsevier.js"></script>
	<script>
		$(document).foundation();
	</script>
	<%@include file="script_files.jsp"%>
</body>
</html>


