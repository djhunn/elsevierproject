
<div class="custom-footer">
	<hr />
	<div class="social-media-links">
		<a href="https://www.facebook.com" class="fa fa-facebook"
			target="_blank"></a> <a href="https://www.twitter.com"
			class="fa fa-twitter" target="_blank"></a> <a
			href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"
			class="fa fa-youtube" target="_blank"></a> <a
			href="https://www.google.com" class="fa fa-google" target="_blank"></a>
	</div>
	<div class="row">
		<p align="center"><small>&copy; Bookspace 2017</small></p>
	</div>
</div>