<!doctype html>
<%@page import="java.util.Map"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.qa.models.Book"%>
<%@page import="com.qa.models.Customer"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Checkout</title>
<%@include file="style_files.jsp"%>


</head>
<body class="main">

	<%@include file="navigation.jsp"%>
	<div class="whitebg">
		<%
			NumberFormat formatter = new DecimalFormat("#0.00");
		%>

		<br>
		<div class="row columns">
			<nav aria-label="You are here:" role="navigation">
				<ul class="breadcrumbs">

					<li><a href="/">Home</a></li>
					<li><span class="show-for-sr">Current: </span> Shipping
						Details</li>
				</ul>
			</nav>
		</div>

		<form action="/checkoutProcess" method="post">

			<%
				double orderTotal = (Double) request.getAttribute("order_total");
			%>

			<%
				if (c != null) {
			%>


			<div class="row columns">
				<h2>Shipping Details</h2>
				<br>
				<h6>Please enter your shipping address.</h6>
			</div>


			<div class="row" id="shippingEnter">
				<div class="small-12 medium-6 columns" id="shippingDetails">
					<div class="row" id="contactDetails">
						<div class="small-12 medium-6 columns">
							<label> First name * </label><input type="text"
								placeholder="Enter your first name" name="firstName"
								id="firstName" size="30" required /> <label> Last name *
							</label><input type="text" placeholder="Enter your last name"
								name="lastName" id="lastName" size="30" required />
						</div>

						<div class="small-12 medium-6 columns">
							<label> Phone Number</label><input type="number"
								placeholder="Enter your phone number" name="phone" id="phone"
								size="30" required /> <label> Email * </label><input
								type="email" placeholder="Enter your email" name="email"
								id="email" size="30" required />
						</div>
					</div>

					<div class="row column" id="addressDetails">
						<div class="row">
							<div class="small-12 columns">
								<label> Address Line 1 * </label> <input type="text"
									placeholder="Enter the first line of your address"
									name="addressLine1" id="addressLine1" size="30" required />
							</div>
						</div>
						<div class="row">
							<div class="small-12 columns">
								<label> Address Line 2 * </label><input type="text"
									placeholder="Enter the second line of your address"
									name="addressLine2" id="addressLine2" size="30" required />
							</div>
						</div>
						<div class="row" id="cityPostcodeCounty">
							<div class="small-4 columns">
								<label>City/Town * </label> <input type="text"
									placeholder="Enter your city" name="city" id="city" size="30"
									required />
							</div>
							<div class="small-4 columns">
								<label>County * </label> <input type="text"
									placeholder="Enter your county" name="county" id="county"
									size="30" required />
							</div>
							<div class="small-4 columns end">
								<label>Postcode * </label> <input type="text"
									placeholder="Enter your postcode" name="postcode" id="postcode"
									size="30" required />
							</div>


						</div>
					</div>

				</div>


				<div class="small-12 medium-6 columns" id="buttonArea">

					<div class="row" id="orderSummary">
						<div class="small-6 columns end">
							<div class="row columns">
								<label for="middle-label" class="middle"><strong>VAT
										(inc.):</strong> �<%=formatter.format(orderTotal * 0.20)%></label>
							</div>
							<div class="row columns">
								<label for="middle-label" class="middle"><strong>Order
										Total:</strong> �<%=formatter.format(orderTotal)%></label>
							</div>



						</div>

					</div>
				</div>

				<div class="small-12 medium-6 columns" id="LoggedIn">

					<input type="hidden" name="order_total" value="<%=orderTotal%>" />
					<div class="row">
						<div class="small-6 columns end">
							<input type="submit" class="button large expanded"
								value="Checkout" />
						</div>
					</div>
				</div>

			</div>
		</form>
		<%
			}
		%>
		<%
			if (c == null) {
		%>

		<div class="row columns" id="needToLogin">

			<h3 >You are not logged in.</h3>
			<p >To make purchases on Bookspace, you must
				log in.</p>
		</div>

		<div class="row">

			<div class="small-6 columns end">

				<a href="/loginThroughCheckout?order_total=<%=orderTotal%>"
					class="button large expanded text-center">Login</a>
			</div>

		</div>


		<%
			}
		%>

		<%@include file="script_files.jsp"%>
		<%@include file="custom_footer.jsp"%></div>
</body>
</html>


