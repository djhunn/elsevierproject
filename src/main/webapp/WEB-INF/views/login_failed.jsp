<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bookspace | Login Failed</title>
<%@include file="style_files.jsp" %>
</head>
<body class="main">
<%session.invalidate(); %>
  <%@include file="navigation.jsp" %>



<div class="whitebg"><br>
<div class="row column text-center">
         
        <h3>Login failed: incorrect account details</h3>
       </div>
<div class="row">
<div class="small-6 small-centered text-center columns">
<a href="/login" class="button large"> Click here to try again
			</a>
</div>
</div>
    <%@include file="script_files.jsp" %>
    <%@include file="custom_footer.jsp"%>
    </div>
</body>
</html>