package com.qa.services;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qa.models.Book;
import com.qa.models.Customer;
import com.qa.models.Order;

@Repository
public interface OrderService extends CrudRepository<Order,Integer>{
	@Query("select c from Customer c where c.email = :email and c.password = :password")
	public Customer loginProcess(@Param("email") String email,@Param("password") String password);
	
	@Query("select o from Order o where o.customer = :c")
	public List<Order> getCustomerOrdersById(@Param("c") Customer c);
	
}
