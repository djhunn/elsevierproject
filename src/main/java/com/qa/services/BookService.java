package com.qa.services;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qa.models.Book;

@Repository
public interface BookService extends CrudRepository<Book, Integer>{
	@Query("SELECT b from Book b WHERE (b.title LIKE %:title%)")
    public List<Book> searchBook (@Param("title") String title)  ;
	
	@Query("SELECT b from Book b WHERE (b.bookId = :bookid)")
    public Book getBookById (@Param("bookid") int bookId)  ;
}
