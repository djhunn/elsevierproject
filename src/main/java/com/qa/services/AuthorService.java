package com.qa.services;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qa.models.Author;

@Repository
public interface AuthorService extends CrudRepository<Author,Integer>
{
    @Query(value="SELECT author_id, about_author, affiliations, author_name, expertise "
            + "FROM Author "
            + "JOIN book_authors "
            + "ON book_authors.authors_author_id = Author.author_id "
            + "WHERE book_authors.book_book_id  = :book_id", nativeQuery = true)
    public List<Author> findAuthors(@Param("book_id") int bookId);
}