package com.qa.services;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.qa.models.Address;

public interface AddressService extends CrudRepository<Address,Integer>
{
	@Modifying
	@Transactional
	@Query("UPDATE Address a set a.addressLine1 = :addressLine1,a.addressLine2 = :addressLine2,a.city = :city,a.postcode = :postcode,a.county= :county,a.phone = :phone WHERE a.customerId = :customerId and a.addressType = :addressType")
	public int updateBillingAddress(@Param("addressLine1") String addressLine1,
			@Param("addressLine2") String addressLine2,
			@Param("city") String city,
			@Param("postcode") String postcode,
			@Param("county") String county,
//			@Param("country") String country,a.country = :country
			@Param("phone") String phone,
			@Param("customerId") int customerId,
			@Param("addressType") String addressType
			);
	
	@Query("SELECT a from Address a WHERE a.customerId = :customerId and a.addressType = :addressType")
	public Address findAddressByType(@Param("customerId") int customerId,@Param("addressType") String addressType);
}
