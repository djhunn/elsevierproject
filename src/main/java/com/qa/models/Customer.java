package com.qa.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.apache.tomcat.jni.Address;

@Entity
public class Customer {

	@Id
	@GeneratedValue
	private int customerId;
	
	private String firstName;
	
	private String lastName;
	
	private String email;
	
	private String password;
	
	private String passwordC;
	
	@OneToMany(mappedBy="customer", fetch=FetchType.EAGER)
	private List<Order> orders;
	 	
		public List<Order> getOrders() {
			return orders;
		}
	 
		public void setOrders(List<Order> orders) {
			this.orders = orders;
		}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPasswordC() {
		return passwordC;
	}

	public void setPasswordC(String passwordC) {
		this.passwordC = passwordC;
	}
	
	
	
}
