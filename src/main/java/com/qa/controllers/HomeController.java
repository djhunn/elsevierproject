package com.qa.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.qa.models.Book;
import com.qa.models.Customer;
import com.qa.models.Shipping;
import com.qa.services.BookService;
import com.qa.services.CustomerService;

@Controller
@SessionAttributes(names={"books","cart_items","logged_in_customer","Address"})
public class HomeController {

	@Autowired
	BookService bookService;
	
	@Autowired
	CustomerService customerService;
	
	@RequestMapping("/")
	public ModelAndView indexPage(HttpServletRequest request)
	{
		
		ArrayList<Book> cartItems = null;
		
		HttpSession session = request.getSession();
		
		Object items = session.getAttribute("cart_items");
		
		if(items!=null)
		{
			cartItems = (ArrayList<Book>) items;
		}else
		{
			cartItems = new ArrayList<Book>();
		}
		
	
		Iterable<Book> books = bookService.findAll();
		
		ModelAndView modelAndView = new ModelAndView("index","books",books);
		
		modelAndView.addObject("cart_items",cartItems);
		return modelAndView;
		
	}
	
	@RequestMapping("/aboutUs")
	public ModelAndView aboutUs()
	{
		ModelAndView modelAndView = new ModelAndView("aboutUs");
	
	    return modelAndView;
	}
	
	@RequestMapping("/cart_details")
	public ModelAndView cartDetails()
	{
		ModelAndView modelAndView = new ModelAndView("cart_details");
	
	    return modelAndView;
	}

	@RequestMapping("/bookPortal")
	public ModelAndView bookPortal()
	{
		Iterable<Book> books = bookService.findAll();
		ModelAndView modelAndView = new ModelAndView("bookPortal","books",books);
	
	    return modelAndView;
	}
	
	
	@RequestMapping("contact")
	public ModelAndView contact()
	{
		ModelAndView modelAndView = new ModelAndView("contact");
	
	    return modelAndView;
	}
	

	@RequestMapping("/login")
	public ModelAndView login()
	{
		ModelAndView modelAndView = new ModelAndView("login");
	
	    return modelAndView;
	}
	
	@RequestMapping("/login_failed")
	public ModelAndView login_failed()
	{
		ModelAndView modelAndView = new ModelAndView("login_failed");
	
	    return modelAndView;
	}
	
	
	@RequestMapping("/register")
	public ModelAndView register()
	{
		ModelAndView modelAndView = new ModelAndView("register");
	
	    return modelAndView;
	}
	
	
	

	@RequestMapping("/registerProcess")
	public ModelAndView registerProcess(@ModelAttribute("Customer") Customer customer)
	{
		ModelAndView modelAndView  = null;	
		Pattern ptr = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
		Matcher emailValidate = ptr.matcher(customer.getEmail());
		
		if(customer!=null && emailValidate.matches() && customer.getPasswordC().equals(customer.getPassword()) 
				&& customer.getFirstName().length() > 0 && customer.getLastName().length() > 0 && customer.getPassword().length() > 0){
			Iterable<Customer> customers = customerService.findAll();
			if (!customers.iterator().hasNext()) {
				customerService.save(customer);
				modelAndView = new ModelAndView("registration_success");
			}
			for(Customer i:customers) {
				if(customer.getEmail().equals(i.getEmail())) {
					System.out.println("I am in line 131");
					customerService.delete(customer);
					modelAndView = new ModelAndView("registration_failed");
					
				}
				else {
					System.out.println("I am in line 137");
					customerService.save(customer);
					modelAndView = new ModelAndView("registration_success");	
				}			
			}
		}
		else
		{
			customerService.delete(customer);
			modelAndView = new ModelAndView("registration_failed");
		}
	  		
		return modelAndView;
	}
	
	@RequestMapping("/loginProcess")
	public ModelAndView loginProcess(@RequestParam("email") String email,
										@RequestParam("password") String password)
	{
		
		ModelAndView modelAndView  = null;
		
		System.out.println("Email is "+email);
		
		
		System.out.println("Password is "+password);
		
		
		Customer c = customerService.loginProcess(email, password);
	  
		if(c!=null)
		{
			System.out.println("Success");
	  		modelAndView = new ModelAndView("customer_home","logged_in_customer",c);
		}
		else
		{
			System.out.println("Failure");
			modelAndView = new ModelAndView("login_failed");
		}
	  		
		return modelAndView;
	}
	
	@RequestMapping("/loginProcessThroughCheckout")
	public ModelAndView loginProcessThroughCheckout(@RequestParam("email") String email,
										@RequestParam("password") String password)
	{
		
		ModelAndView modelAndView  = null;

		Customer c = customerService.loginProcess(email, password);
	  
		if(c!=null)
		{
			System.out.println("Success");
	  		modelAndView = new ModelAndView("cart_details","logged_in_customer",c);
		}
		else
		{
			System.out.println("Failure");
			modelAndView = new ModelAndView("login_failed");
		}
	  		
		return modelAndView;
	}
	
@RequestMapping("/logout")
	public ModelAndView logout()
	{

		ModelAndView modelAndView = new ModelAndView("logout");
	
	    return modelAndView;
	}
	
	
	
	@RequestMapping("/profile")
	public ModelAndView profile(@ModelAttribute("logged_in_customer") Customer loggedInCustomer)
	{
		ModelAndView modelAndView = new ModelAndView("profile","logged_in_customer",loggedInCustomer);
	
	    return modelAndView;
	}
	
	
	
	
	@RequestMapping("/updateProfile")
	public ModelAndView updateProfile(@ModelAttribute("logged_in_customer") Customer loggedInCustomer, @ModelAttribute("Customer") Customer customer)
	{
		//Sets up email validation
		Pattern ptr = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
		Matcher emailValidate = ptr.matcher(customer.getEmail());
	
	
	if(customer!=null && emailValidate.matches() && customer.getFirstName().length() > 0 && customer.getLastName().length() > 0 ){
		
		ModelAndView modelAndView  = null;
		System.out.println("went down if");
		

		System.out.println("ID "+loggedInCustomer.getCustomerId());
		System.out.println("Name"+loggedInCustomer.getFirstName());
		System.out.println("Email"+loggedInCustomer.getEmail());
		
		
		int recordsUpdated = customerService.updateCustomer(loggedInCustomer.getFirstName(),
				loggedInCustomer.getLastName(),
				loggedInCustomer.getEmail(),
				loggedInCustomer.getPassword(),
				loggedInCustomer.getPasswordC(),
				loggedInCustomer.getCustomerId());
				
		
		if(recordsUpdated>0)
		{
			Customer c  = customerService.findOne(loggedInCustomer.getCustomerId());
		
			System.out.println("After update ");

			System.out.println("ID "+c.getCustomerId());
			System.out.println("Name"+c.getFirstName());
			System.out.println("Email"+c.getEmail());
			
			
			modelAndView = new ModelAndView("profile","logged_in_customer",c);
		}
		else
		{
			modelAndView = new ModelAndView("profile","logged_in_customer",loggedInCustomer);
		}
	
		return modelAndView;}
	else {
		ModelAndView modelAndView = new ModelAndView("profile");
		return modelAndView;
	}
	}
	
	
	@RequestMapping("/addressBook")
	public ModelAndView addressBook(@ModelAttribute("logged_in_customer") Customer loggedInCustomer)
	{
		ModelAndView modelAndView = new ModelAndView("address_book","logged_in_customer",loggedInCustomer);
	
	    return modelAndView;
	}
	
	@RequestMapping("/registered_user_agreement")
	public ModelAndView registered_user_agreement()
	{
		ModelAndView modelAndView = new ModelAndView("registered_user_agreement");
		return modelAndView;
	}
	
	
	
}
