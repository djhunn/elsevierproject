package com.qa.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.qa.models.Order;
import com.qa.models.Customer;
import com.qa.models.Book;
import com.qa.services.AddressService;
import com.qa.services.OrderService;

@Controller
@SessionAttributes(names = { "logged_in_customer", "customer_orders" })
public class OrderController {

	@Autowired
	private OrderService orderService;

	@Autowired
	private AddressService addressService;

	@RequestMapping("/myOrders")
	public ModelAndView myOrders(@ModelAttribute("logged_in_customer") Customer loggedInCustomer) {
		loggedInCustomer.setOrders(orderService.getCustomerOrdersById(loggedInCustomer));
		List<Order> orders = loggedInCustomer.getOrders();
		for (Order o : orders) {
			o.setBillingAddress(addressService.findOne(o.getBillingAddressId()));
			o.setShippingAddress(addressService.findOne(o.getShippingAddressId()));
			o.setBookQuantity(getOrderBook(o.getBooks()));
		}
		Collections.sort(orders, new Comparator<Order>() {
			public int compare(Order o1, Order o2) {
				DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
				Date o1Date;
				Date o2Date;
				try {
					o1Date = dateFormat.parse(o1.getDate());
					o2Date = dateFormat.parse(o2.getDate());
				} catch (ParseException e) {
					return o1.getDate().compareTo(o2.getDate());
				}
				return o2Date.compareTo(o1Date);
			}
		});
		ModelAndView modelAndView = new ModelAndView("order_history", "logged_in_customer", loggedInCustomer);
		modelAndView.addObject("customer_orders", orders);
		return modelAndView;
	}

	// Business method

	public Map<Book, Integer> getOrderBook(List<Book> books) {
		Map<Book, Integer> bookQuant = new HashMap<Book, Integer>();
		for (Book b : books) {
			Integer i = bookQuant.get(b);
			if (i == null) {
				bookQuant.put(b, 1);
			} else {
				bookQuant.put(b, bookQuant.get(b) + 1);
			}
		}
		return bookQuant;

	}
}
