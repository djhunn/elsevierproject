package com.qa.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.qa.models.Address;
import com.qa.models.Book;
import com.qa.models.Customer;
import com.qa.models.Order;
import com.qa.models.Shipping;
import com.qa.services.AddressService;
import com.qa.services.BookService;
import com.qa.services.OrderService;

@SessionAttributes(names = { "book_counts", "logged_in_customer", "shipping_address_id", "customer_orders" })
@Controller
public class CheckoutController {

	@Autowired
	private AddressService addressService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private BookService bookService;

	@RequestMapping("/checkoutProcess")
	public ModelAndView checkoutProcess(@ModelAttribute("Address") Address address,
			@ModelAttribute("Shipping") Shipping shipping,
			@ModelAttribute("book_counts") Map<Integer, Integer> bookCounts,
			@RequestParam("order_total") double orderTotal, @ModelAttribute("logged_in_customer") Customer customer) {
		ModelAndView modelAndView = new ModelAndView("payment_form", "order_total", orderTotal);
		modelAndView.addObject("shipping_address", shipping);
		modelAndView.addObject("shipping_address_id", shipping.getShippingId());
		modelAndView.addObject("order_total", orderTotal);
		modelAndView.addObject("book_counts", bookCounts);
		address.setAddressType("shipping");
		address.setCustomerId(customer.getCustomerId());
		addressService.save(address);
		modelAndView.addObject("shipping_address_id", address.getAddressId());
		return modelAndView;
	}

	@RequestMapping("/loginThroughCheckout")
	public ModelAndView loginThroughCheckout(@ModelAttribute("book_counts") Map<Integer, Integer> bookCounts,
			@RequestParam("order_total") double orderTotal) {

		ModelAndView modelAndView = new ModelAndView("login_through_checkout", "order_total", orderTotal);

		modelAndView.addObject("order_total", orderTotal);
		modelAndView.addObject("book_counts", bookCounts);
		return modelAndView;
	}

	@RequestMapping("/paymentProcess")
	public ModelAndView paymentProcess(@ModelAttribute("shipping_address_id") int shippingId,
			@ModelAttribute("Address") Address address, @ModelAttribute("book_counts") Map<Integer, Integer> bookCounts,
			@RequestParam("order_total") double orderTotal, @ModelAttribute("logged_in_customer") Customer customer) {

		address.setAddressType("billing");
		address.setCustomerId(customer.getCustomerId());
		addressService.save(address);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		dateFormat.format(date);
		Order newOrder = new Order();
		newOrder.setCustomer(customer);
		newOrder.setBillingAddressId(address.getAddressId());
		newOrder.setShippingAddressId(shippingId);
		newOrder.setTotal(orderTotal);
		newOrder.setDate(date.toString());
		this.orderService.save(newOrder);

		List<Book> bookList = new ArrayList<Book>();
		for (Map.Entry<Integer, Integer> entry : bookCounts.entrySet()) {
			for (int j = 0; j < entry.getValue(); j++) {
				bookList.add(this.bookService.getBookById(entry.getKey()));
			}
		}
		for (Book bo : bookList) {
			bo.getOrders().add(newOrder);
			bookService.save(bo);
		}
		customer.setOrders(orderService.getCustomerOrdersById(customer));
		return new ModelAndView("redirect:/myOrders", "logged_in_customer", customer);
	}

}
