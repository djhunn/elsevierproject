package com.qa.controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.qa.models.Author;
import com.qa.models.Book;
import com.qa.services.AuthorService;
import com.qa.services.BookService;

@Controller
@SessionAttributes(names = { "books", "cart_items", "book_counts", "filtered_books", "search_books", "book_authors" })
public class BookController {

	@Autowired
	BookService bookService;

	@Autowired
	AuthorService authorService;

	@RequestMapping("/bookDetails")
	public ModelAndView bookDetails(@ModelAttribute("books") Iterable<Book> books, @RequestParam("bookId") int bookId) {
		Book book = findBookById(books, bookId);
		List<Author> bookAuthors = authorService.findAuthors(bookId);
		book.setAuthors(bookAuthors);
		ModelAndView modelAndView = new ModelAndView("book_details", "book", book);
		modelAndView.addObject("books", books);
		// modelAndView.addObject("book_authors",bookAuthors);
		return modelAndView;

	}

	// Normal Search Bar
	@RequestMapping("/findBooks")
	public ModelAndView findBooks(@RequestParam("search-input") String searchInput) throws SQLException {

		List<Book> searchItem = bookService.searchBook(searchInput);

		ModelAndView modelAndView = new ModelAndView("search_results");

		modelAndView.addObject("search_books", searchItem);

		return modelAndView;

	}

	// Registered User Search
	@RequestMapping("/findBooks2")
	public ModelAndView findBooks2(@RequestParam("search-input-2") String searchInput2) {

		List<Book> searchItem = bookService.searchBook(searchInput2);

		Iterable<Book> booksTest = this.bookService.findAll();

		for (Book b : booksTest) {
			List<Author> bookAuthors = authorService.findAuthors(b.getBookId());
			b.setAuthors(bookAuthors);
		}

		if (searchInput2.length() > 0) {

			for (Book bo : booksTest) {
				if (bo.getTitle().toLowerCase().indexOf(searchInput2.toLowerCase()) >= 0) {
					searchItem.add(bo);
				}
			}

			for (Book bo : booksTest) {
				for (Author a : bo.getAuthors()) {
					if (a.getAuthorName().toLowerCase().indexOf(searchInput2.toLowerCase()) >= 0) {
						searchItem.add(bo);
					}
				}
			}

			for (Book bo : booksTest) {
				if (bo.geteBookISBN().toLowerCase().indexOf(searchInput2.toLowerCase()) >= 0) {
					searchItem.add(bo);
				}
			}

			for (Book bo : booksTest) {
				if (bo.getPaperISBN().indexOf(searchInput2.toLowerCase()) >= 0) {
					searchItem.add(bo);
				}
			}

			ModelAndView modelAndView = new ModelAndView("search_results");

			modelAndView.addObject("search_books", searchItem);

			return modelAndView;

		} else {

			ModelAndView noResults = new ModelAndView("search_results");

			List<Book> test = new ArrayList<Book>();

			noResults.addObject("search_books", test);

			return noResults;

		}
	}

	private String toLowerCase(String title) {
		// TODO Auto-generated method stub
		return null;
	}

	@RequestMapping("/addToCart")
	public ModelAndView addToCart(@ModelAttribute("books") Iterable<Book> books, @RequestParam("bookId") int bookId,
			@ModelAttribute("cart_items") ArrayList<Book> cartItems) {

		Book book = findBookById(books, bookId);

		ModelAndView modelAndView = new ModelAndView("cart_updated", "cart_items", cartItems);

		cartItems.add(book);

		modelAndView.addObject("books", books);
		return modelAndView;

	}

	@RequestMapping("/viewCart")
	public ModelAndView viewCart(@ModelAttribute("books") Iterable<Book> books,
			@ModelAttribute("cart_items") ArrayList<Book> cartItems) {

		ModelAndView modelAndView = null;

		ArrayList<Integer> bookIds = loadBookIds(cartItems);

		Map<Integer, Integer> bookCounts = bookCounts(bookIds);

		ArrayList<Book> filteredBooks = filteredBookList(books, bookCounts);

		if (cartItems.size() != 0) {

			modelAndView = new ModelAndView("cart_details", "cart_items", cartItems);
			modelAndView.addObject("book_counts", bookCounts);
			modelAndView.addObject("filtered_books", filteredBooks);

		} else {
			modelAndView = new ModelAndView("cart_empty", "cart_items", cartItems);
			modelAndView.addObject("book_counts", bookCounts);
			modelAndView.addObject("filtered_books", filteredBooks);
		}

		return modelAndView;

	}

	@RequestMapping("/removeFromCart")
	public ModelAndView removeFromCart(@ModelAttribute("filtered_books") ArrayList<Book> cartItems,
			@RequestParam("bookId") int bookId) {

		cartItems = removeBookById(cartItems, bookId);

		ModelAndView modelAndView = null;

		if (cartItems.size() != 0) {

			modelAndView = new ModelAndView("cart_details", "cart_items", cartItems);
		} else {
			modelAndView = new ModelAndView("cart_empty", "cart_items", cartItems);
		}

		return modelAndView;

	}

	public ArrayList<Integer> loadBookIds(ArrayList<Book> cartItems) {

		ArrayList<Integer> bookIds = new ArrayList<>();

		for (Book book : cartItems) {
			bookIds.add(book.getBookId());
		}

		return bookIds;

	}

	// Some business methods

	public Book findBookById(Iterable<Book> books, int bookId) {

		Book book = null;

		for (Book b : books) {
			if (b.getBookId() == bookId) {
				book = b;
			}
		}
		return book;

	}

	/*
	 * public ArrayList<Book> loadBooksIntoCart(Iterable<Book>
	 * books,ArrayList<Integer> bookIds) {
	 * 
	 * ArrayList<Book> bookList = new ArrayList<>();
	 * 
	 * for(Book book : books) { for(int bookId : bookIds)
	 * if(bookId==book.getBookId()) bookList.add(book); }
	 * 
	 * return bookList; }
	 */

	public boolean findBookInCart(ArrayList<Integer> cartItems, int bookId) {

		// Set<Book> books = cartItems.keySet();
		//
		//
		// for(Book b : books)
		// {
		// if(b.getBookId()==bookId)
		// {
		// bookFound = true;
		// }
		// }

		return cartItems.contains(bookId);

	}

	public ArrayList<Book> removeBookById(ArrayList<Book> books, int bookId) {

		books.removeIf(b -> b.getBookId() == bookId);

		return books;

	}

	public Map<Integer, Integer> bookCounts(ArrayList<Integer> bookIds) {

		Map<Integer, Integer> map = new HashMap<Integer, Integer>();

		for (int bookId : bookIds) {
			Integer count = map.get(bookId);
			map.put(bookId, (count == null) ? 1 : count + 1);
		}

		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
			System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
		}

		return map;
	}

	public ArrayList<Book> filteredBookList(Iterable<Book> books, Map<Integer, Integer> map) {
		ArrayList<Book> filteredBooks = new ArrayList<>();

		for (Book book : books) {
			for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
				Integer bookId = entry.getKey(); // Get book ID
				if (bookId == book.getBookId())
					filteredBooks.add(book);
			}
		}

		System.out.println("Number of filtered items " + filteredBooks.size());
		return filteredBooks;

	}

}
