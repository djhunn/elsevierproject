package com.qa.controllers;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.qa.models.Book;
import com.qa.models.Request;

import com.qa.services.RequestService;

@Controller 
public class RequestController {
	@Autowired
	private RequestService requestService;
	
	@RequestMapping("/requests")
	public ModelAndView requests(@ModelAttribute("request") Request request) {
		requestService.save(request);
		
		ModelAndView modelAndView = new ModelAndView("index");
		
		return modelAndView ;}

}
