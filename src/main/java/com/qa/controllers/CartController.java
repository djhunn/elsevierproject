package com.qa.controllers;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.qa.models.Book;
@SessionAttributes(names={"book_counts", "books", "cart_items"})
@Controller
public class CartController {

	@RequestMapping("/updatePrice")
	public ModelAndView bookDetails(@RequestParam("price") double price,@RequestParam("quantity") int quantity)
	{
		double totalPrice = price * quantity;
		
		System.out.println("Total price is "+price);
		ModelAndView modelAndView = new ModelAndView("return_price","total_price",totalPrice);
		
		return modelAndView;
		
	}
	
	@RequestMapping("/updateCart")
	public ModelAndView addToCart(@ModelAttribute("books") Iterable<Book> books,@ModelAttribute("book_counts") Map<Integer,Integer> bookCounts, @RequestParam("bookId") int bookId,
			@ModelAttribute("cart_items") ArrayList<Book> cartItems,
			@RequestParam(value="quantity", required=true) int quantity) {
		
		
		Book book = findBookById(books, bookId);
		bookCounts.put(bookId, quantity);
		System.out.println(bookCounts);
		ModelAndView modelAndView = new ModelAndView("cart_details", "cart_items", cartItems);
		cartItems.remove(book);
		System.out.println("bookId "+ bookId);
		System.out.println("cartItems "+ cartItems);
		
		while(quantity>0) {
			
		cartItems.add(book);
		modelAndView.addObject("cart_items", cartItems);
		modelAndView.addObject("books", books);
		quantity--;
		System.out.println(quantity);
		System.out.println(cartItems);
		
		}
		
		return modelAndView;

	}
	
	
	
	@RequestMapping("/checkout")
	public ModelAndView checkoutForm(@ModelAttribute("book_counts") Map<Integer,Integer> bookCounts,@RequestParam("order_total") double orderTotal)
	{
//		quantity;
		ModelAndView modelAndView = new ModelAndView("checkout","order_total",orderTotal);
		modelAndView.addObject("book_counts", bookCounts);
		return modelAndView;
		
	}
	
	public Book findBookById(Iterable<Book> books, int bookId) {

		Book book = null;

		for (Book b : books) {
			if (b.getBookId() == bookId) {
				book = b;
			}
		}
		return book;

	}
	
	
	
}
